use clap::{Arg, Command};
use rand::seq::SliceRandom;
use rand::{thread_rng, Rng};

enum LottoType {
    MegaMillions,
    Powerball,
    SuperLotto,
}

impl LottoType {
    fn get_parameters(&self) -> (u32, u32, Option<u32>) {
        match self {
            LottoType::MegaMillions => (70, 5, Some(25)),
            LottoType::Powerball => (69, 5, Some(26)),
            LottoType::SuperLotto => (47, 5, Some(27)),
        }
    }
}

struct LottoGame {
    game_type: LottoType,
    game_name: String,
    plays: u32,
    numbers: Vec<Vec<u32>>,
    power_number: Vec<Vec<u32>>,
}

impl LottoGame {
    fn new(game_type: LottoType, game_name: String, plays: u32) -> Self {
        LottoGame {
            game_type,
            game_name,
            plays,
            numbers: Vec::new(),
            power_number: Vec::new(),
        }
    }

    fn generate_numbers(&mut self) {
        let (range, count, power_play_range) = self.game_type.get_parameters();
        let mut rng = thread_rng();

        for _ in 0..self.plays {
            let mut main_numbers: Vec<u32> = (1..=range).collect();
            main_numbers.shuffle(&mut rng);
            main_numbers.truncate(count as usize);

            main_numbers.sort();
            self.numbers.push(main_numbers);

            if let Some(power_range) = power_play_range {
                let power_ball: u32 = rng.gen_range(1..=power_range);
                self.power_number.push(vec![power_ball; 1]);
            }
        }
    }

    fn print_results(&self) {
        for (play, power_number) in self.numbers.iter().zip(&self.power_number) {
            println!(
                "{:<14} {:<22} Big Ball: {:?}",
                self.game_name,
                format!("{:?}", play),
                power_number
            );
        }
    }
}

fn main() {
    let matches = Command::new("California Lottery Number Generator")
        .author("rasinnovation")
        .version("0.1.0")
        .about("This program generates lottery pairs for compatible games.")
        .arg(Arg::new("MEGA")
            .short('m')
            .long("mega")
            .help("Enter number of desired plays for Mega Millions game. (default = 0, limit = 5)")
            .value_name("PLAYS")
            .num_args(1))
        .arg(Arg::new("POWERBALL")
            .short('p')
            .long("power")
            .help("Enter number of desired plays for Powerball game. (default = 0, limit = 5")
            .value_name("PLAYS")
            .num_args(1))
        .arg(Arg::new("SUPERLOTTO")
            .short('s')
            .long("super")
            .help("Enter number of desired plays for Super Lotto game. (default = 0, limit = 5")
            .value_name("PLAYS")
            .num_args(1))
        .get_matches();

    if let Some(plays_str) = matches.get_one::<String>("MEGA") {
        let plays = plays_str.parse::<u32>().unwrap_or(0);
        let mut game = LottoGame::new(
            LottoType::MegaMillions,
            String::from("Mega Millions"),
            plays,
        );
        game.generate_numbers();
        game.print_results();
    }

    if let Some(plays_str) = matches.get_one::<String>("POWERBALL") {
        let plays = plays_str.parse::<u32>().unwrap_or(0);
        let mut game = LottoGame::new(LottoType::Powerball, String::from("Powerball"), plays);
        game.generate_numbers();
        game.print_results();
    }

    if let Some(plays_str) = matches.get_one::<String>("SUPERLOTTO") {
        let plays = plays_str.parse::<u32>().unwrap_or(0);
        let mut game = LottoGame::new(LottoType::SuperLotto, String::from("Super Lotto"), plays);
        game.generate_numbers();
        game.print_results();
    }
}
